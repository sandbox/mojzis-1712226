<?php
/**
 * @file
 * Page callbacks as referenced in hook_menu.
 */

/**
 * Show page from defintions per name.
 */
function svg_inpage_display_page($pagename) {
  $out = array();
  $svg_pages = svg_inpage_pages();
  if (isset($svg_pages[$pagename])) {
    $page = $svg_pages[$pagename];
    $svg_string = svg_inpage_svg_string($page['svg file']);
    $out = array(
      '#markup' => $svg_string,
    );
    if (isset($page['js callback'])) {
      $page['js callback']();
    }
  }
  else {
    // Anounce there is a problem.
    drupal_set_message(t("Page name %pagename is not defined.",
    array('%pagename', $pagename)),
    'error');
    // @todo log it.
  }
  return $out;
}

/**
 * Test page to see if it works in your theme.
 */
function svg_inpage_test() {
  $out = array();
  // Read the file.
  // Todo: add a test if file exists.
  $modpath = drupal_get_path('module', 'svg_inpage');
  $svg_file = file($modpath . 'svg_inpage_example/examples/test.svg');

  // Remove the beginning lines.
  // TODO: make this optional / configurable.
  $svg_string = '';
  $found_svg_tag = FALSE;
  foreach ($svg_file as $line) {
    if (substr($line, 0, 4) == '<svg') {
      $found_svg_tag = TRUE;
    }
    if ($found_svg_tag) {
      $svg_string .= $line;
      // TODO: make newline optional here ?
    }
  }

  $out = array(
    '#markup' => $svg_string,
  );
  return $out;
}
